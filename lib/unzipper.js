const path = require('path')
const unzipper = require('unzipper')
const http = require('https')
const fs = require('fs')

module.exports = function(url, name) {
    return new Promise((resolve, reject) => {
        http
        .get(url, function(res) {
            res
                .pipe(unzipper.Parse())
                .on('entry', entry => {

                    p = path.resolve(path.join(name, entry.path.replace('infopack-example-basic-master', '')))

                    console.log('  ' + p)
                    if(entry.type == 'Directory') {
                        fs.mkdirSync(p, { recursive: true })
                    }
                    if(entry.type == 'File') {
                        entry.pipe(fs.createWriteStream(p))
                    }
                    
                })
                .on('close', () => {
                    console.log('=== DONE!')
                    resolve()
                })
        })
    })
}
